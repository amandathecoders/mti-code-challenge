/*
Longest Word

Have the function LongestWord(sen) take the sen parameter being passed and return the largest word in the string. 
If there are two or more words that are the same length, return the first word from the string with that length. 
Ignore punctuation and assume sen will not be empty.

Use the Parameter Testing feature in the box below to test your code with different arguments.
*/

function LongestWord(sen) { 

    // code goes here 
    // WITH REGEX 
    return sen.split(" ").map(function(word) {
          return word.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/gi, ''); // using regex to ignore punctiation
      }).reduce(function(a, b) {
          return a.length >= b.length ? a : b;
      });


      /* Other method that can be used (WITHOUT REGEX)

      var punct = '\.,-/#!$%^&*;:{}=-_`~()'.split('');

      var words = sen.split(" ").map(function(item) {
          return item.split('').filter(function(char) {
              return punct.indexOf(char) === -1;
          }).join('');
      });
  
      return words.reduce(function(a, b) {
          return a.length > b.length ? a : b;
      });

      */
  
  } 
  // keep this function call here 
  console.log(LongestWord("fun&!! time"));