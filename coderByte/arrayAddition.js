/*
Have the function ArrayAddition(arr) take the array of numbers stored in arr and return the string true if any combination of numbers in the array 
can be added up to equal the largest number in the array, otherwise return the string false. 
For example : if arr contains [4,6, 23, 10, 1, 3] the output should return true because 4 + 6 + 10 + 3 = 23. 
The array will not be empty, will not contain all the same elements, and may contain negative numbers.

test case :
input = [5, 7, 16, 1, 2]
output = "false"

input = [3, 5, -1, 8, 12]
output = "true"
*/

function arrayAddition(arr) {

    let largestNum = arr.sort(function(a,b) { return a - b }).pop();

    return checkCombinations(arr, largestNum);
}

function checkCombinations(arr, target) {

    if(arr.length === 0) {

        return target === 0

    } else {

        let possible = arr[0];
        arr = arr.slice(1);

        return checkCombinations(arr, target - possible) || checkCombinations(arr, target)
    }
}

// keep this function call here (test case)
console.log(arrayAddition([5, 7, 16, 1, 2]))
console.log(arrayAddition([4, 6, 23, 10, 1, 3]))
console.log(arrayAddition([3, 5, -1, 8, 12]))