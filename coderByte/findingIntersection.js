/*
Find Intersection

Have the function FindIntersection(strArr) read the array of strings stored in strArr which will contain 2 elements: 
the first element will represent a list of comma-separated numbers sorted in ascending order, 
the second element will represent a second list of comma-separated numbers (also sorted). 
Your goal is to return a comma-separated string containing the numbers that occur in elements of strArr in sorted order. 
If there is no intersection, return the string false.

Examples:

Input: ["1, 3, 4, 7, 13", "1, 2, 4, 13, 15"]
Output: 1,4,13

Input: ["1, 3, 9, 10, 17, 18", "1, 4, 9, 10"]
Output: 1,9,10

*/

// with time complexity O(1) (Big O notation)

function FindIntersection(strArr) { 

    // code goes here  
    const array = strArr.map(str => str.split(", "));
    const firstArray = array[0];
    const secondArray = array[1];
  
    let match = {}
    let result = []
  
    firstArray.forEach(num => match[num] = true)
  
    secondArray.forEach(num => {
      if(match[num] == true) {
        result.push(num)
      } 
    })
    
    if(result.length > 0) {
      return result.join(",")
    }
  
    return false; 
  
  } 
  // keep this function call here 
  console.log(FindIntersection(["1, 3, 4, 7, 13", "1, 2, 4, 13, 15"]));