/*
have the function BinaryConverter(str) return decimal form of the binary value. FOr example: if 101 is passed return 5, or if 1000 is passed return 8.

test case:
input = "100101"
output = "37"

input = "011"
output = "3"

radix => means base
*/
function binaryConverter(str) {

    // parseInt(string[, radix])
    return parseInt(str, 2);
}

//keep this function call (test case)
console.log(binaryConverter("100101"))
console.log(binaryConverter("011"))
console.log(binaryConverter("01101"))
console.log(binaryConverter("101"))