const whatTimeIsIt = time => {
  // do code here
  const result = time.split(" ")

  // console.log(result)
  const split2 = result[0].split(":")

  const hour = split2[0]
  const minute = split2[1]
  const ampm = result[1] == "pm" ? "afternoon" : "morning"

  const puluhan = minute.split("")[0]
  const satuan = minute.split("")[1]

  const nameOne = [
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
    "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
  ]

  const nameTwo = [
    "one",
    "ten",
    "twenty",
    "thirty",
    "forty",
    "fifthy"
  ]

  if(hour < 12 && minute < 20) {
    return `It's ${nameOne[hour]} past ${nameOne[minute]} in the ${ampm}`
  }
  if(hour < 12 && minute >= 20) {
    return `It's ${nameOne[hour]} past ${nameTwo[puluhan]} ${nameOne[satuan]} in the ${ampm}`
  }
  if(hour == 12 && minute == 0 && ampm == "afternoon") {
    return `It's noon`
  }
  if(hour == 12 && minute == 0 && ampm == "morning") {
    return `It's midnight`
  }


}

// do not change this code below
const test = (testCase, result) => console.log(testCase === result);

test(whatTimeIsIt("12:00 pm"), "It's noon")
test(whatTimeIsIt("3:49 pm"), "It's three past forty nine in the afternoon")
test(whatTimeIsIt('12:00 am'), "It's midnight")
test(whatTimeIsIt("5:31 am"), "It's five past thirty one in the morning")
test(whatTimeIsIt("9:11 am"), "It's nine past eleven in the morning")


test(whatTimeIsIt("10:21 am"), "It's ten past twenty one in the morning")
